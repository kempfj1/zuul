#include <iostream>
#include "command.h"
#include "CommandWords.h"
#include "Parser.h"
#include "Game.h"
#include "Candy.h"
using namespace std;
//Zuul Conversion to C++
//@author Julia Kempf
//@version 2016.10.05

/*void commandTest(){
  //test command() methods
  Command Cmd("BEAN","SPROUT");
  cout << "HERE IS THE COMMAND TEST" << endl;
  if(Cmd.getCommandWord()=="BEAN"){
    cout << "getCommandWord() has passed HECK YEAH" <<endl;
  }
  else{
    cout << "getCommandWord() failed YOU SUCK" << endl;
  }
  if(Cmd.getSecondWord()=="SPROUT"){
    cout << "getSecondWord() has passed YOU DID IT" << endl;
  }
  else{
    cout << "getSecondWord() failed rethink your life" << endl;
  }
}

void commandWordsTest(){
  //test CommandWords() methods
  CommandWords testyMcTest;
  cout << "HERE IS THE COMMANDWORDS TEST" << endl;
  if(testyMcTest.isCommand("go")==1){
    cout<< "isCommand() has passed hooray" << endl;
  }
  else{
    cout<< "isCommand() has failed what are you doing" << endl;
  }
}
*/

int main()
{
  //commandTest();

  //commandWordsTest();

  Game theGame;

  theGame.play();
}
